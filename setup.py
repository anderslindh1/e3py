from setuptools import find_packages, setup

setup(
    name="e3py",
    description="Utilities to interact with e3",
    author="Anders Lindh Olsson",
    author_email="anders.lindholsson@ess.eu",
    url="https://gitlab.esss.lu.se/anderslindh1/e3py",
    license="GPL",
    version="0.10.3",
    python_requires=">=3.6",
    packages=find_packages(),
    entry_points={"console_scripts": "e3py=e3py.e3py:main"},
    install_requires=[
        "python-gitlab",
        "PyYAML",
    ],
)
