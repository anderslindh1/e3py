e3py
====

A collection of python utilities to use with e3.

Requirements
------------

A python-gitlab configuration file if you want to use the `env` subcommand. A private token in this file if you want to access non-public projects and groups.

Quickstart
----------

.. code-block:: sh

    $ pip3 install e3py -i https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple --user
    $ e3py -h

Examples
--------

.. code-block:: sh

    $ e3py module asyn --tags
    7.0.5-3.4.0/4.41.0-5bfc506-20210315T181422
    7.0.5-3.4.0/4.41.0-aef1338-20210310T215127
    7.0.4-3.3.0/4.37.0-a0b085d-20200923T232358
    7.0.4-3.2.0/4.37.0-0ecfb0e-20200715T210150
    7.0.4-3.2.0/4.39.0-625ca13-20200715T185840
    7.0.3.1-3.1.2/R-4.36.0-c192d55-1911121611
    7.0.3-3.1.1/4.33.0-2f239fe-1909112200
    3.15.5-3.0.4/4.33.0-19548d8-1909040017
    3.15.5-3.0.2/4.33.0-b7de942-1811092227
    3.15.5-3.0.0/4.33.0-1811020213
    R0.2.1.0
    3.15.5-3.0.2/4.33.0-1811020209
    R0.2.0.0
    R0.1.0.0
    $ e3py module stream --git-url
    git@gitlab.esss.lu.se:e3/common/e3-stream.git
    $ e3py group --all
    # The top-level group can be accessed as 'e3' (or leave blank)
    # There are 14 subgroups:
    area
    bi
    common
    deprecated
    devices
    ecat
    ifc
    mps
    ps
    psi
    pss
    rf
    ts
    vac
    $ e3py group area --modules
    # The group area contains the following 15 modules:
    e3-ADGraphicsMagick
    e3-ADSpinnaker
    e3-ADURL
    e3-ADPluginCalib
    e3-ADPluginEdge
    e3-ADPointGrey
    e3-ADProsilica
    e3-ADAndor3
    e3-ADGenICam
    e3-ADAndor
    e3-NDDriverStdArrays
    e3-ADCSimDetector
    e3-ADSimDetector
    e3-ADCore
    e3-ADSupport
    $ e3py env essioc
    essioc:
    - 0.4.1+0
    - 0.5.0+0
    $ e3py env asyn --meta
    {'module_diffs': [' M .ci'],
     'module_git_desc': 'R4-41',
     'wrapper_diffs': [' M asyn'],
     'wrapper_git_desc': '7.0.5-3.4.0/4.41.0-5bfc506-20210315T181422',
     'wrapper_url': 'git@gitlab.esss.lu.se:e3/common/e3-asyn.git'}
